# Load DisGeNET v3.0.0.0 :

np get -i -o nanopubs/disgenet-v3.0.0.0.index.nq.gz RAVEKRW0m6Ly_PjmhcxCZMR5fYIlzzqjOWt1CgcwD_77c
np get -c -o nanopubs/disgenet-v3.0.0.0.nq.gz RAVEKRW0m6Ly_PjmhcxCZMR5fYIlzzqjOWt1CgcwD_77c

# Load LIDDI:

np get -i -o nanopubs/liddi.index.nq.gz RA7SuQ0e661LJdKpt5EOS2DKykf1ht9LFmNaZtFSDMrXg
np get -c -o nanopubs/liddi.nq.gz RA7SuQ0e661LJdKpt5EOS2DKykf1ht9LFmNaZtFSDMrXg
