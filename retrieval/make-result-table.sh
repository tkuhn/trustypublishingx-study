#!/bin/bash

echo "RUN,RETRIES,TIME" \
  > results-r.csv
echo "RUN,RETRIES,TIME" \
  > results-u.csv

for C in "r" "u"; do
  for N in {01..20}; do
    R=`cat reports/$C$N.txt \
        | egrep '^Number of retries: ' \
        | sed -r 's/^.* ([0-9]+)$/\1/'`
    M=`cat reports/$C$N.txt \
        | grep -P '^real\t' \
        | sed -r 's/^.*\t(.*)m(.*)s$/\1/'`
    S=`cat reports/$C$N.txt \
        | grep -P '^real\t' \
        | sed -r 's/^.*\t(.*)m(.*)s$/\2/'`
    T=`echo "( $M * 60 ) + $S" | bc`
    echo "$N,$R,$T" \
      >> results-$C.csv
  done
done
