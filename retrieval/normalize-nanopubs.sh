#!/bin/bash

for N in {01..20}; do
  echo "Normalize r$N"
  rapper -i trig -o nquads files/r$N-nanopubs.trig | sort > files/r$N-nanopubs.nq
  echo "Normalize u$N"
  rapper -i trig -o nquads files/r$N-nanopubs.trig | sort > files/u$N-nanopubs.nq
done
