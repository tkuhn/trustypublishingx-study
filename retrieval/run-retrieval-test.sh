#!/bin/bash

echo "Test will start in 60 seconds"
sleep 60

INDEX=RA7SuQ0e661LJdKpt5EOS2DKykf1ht9LFmNaZtFSDMrXg

mkdir -p files

for N in {01..20}; do
  echo "Test run $N, reliable"
  date > reports/r$N.txt
  ( time np get -o files/r$N-nanopubs.trig -e files/r$N-errors.txt -c -r $INDEX ) >> reports/r$N.txt 2>&1
  sleep 300
  echo "Test run $N, unreliable"
  date > reports/u$N.txt
  ( time np get -o files/u$N-nanopubs.trig -e files/u$N-errors.txt -c -r --simulate-unreliable-connection $INDEX ) >> reports/u$N.txt 2>&1
  sleep 300
done
