Trusty Publishing Study: Extension
==================================

This repo contains the files that were used to present the results of the trusty
publishing studies in the extended journal article, as well as the files for the
additional retrieval evaluation.

A pre-print of the extended journal article can be found here:

- [Decentralized provenance-aware publishing with nanopublications](https://peerj.com/preprints/1760/)

This is the original conference paper:

- Tobias Kuhn, Christine Chichester, Michel Dumontier, and Michael Krauthammer.
  Publishing without Publishers: a Decentralized Approach to Dissemination,
  Retrieval, and Archiving of Data. In Proceedings of the 14th International
  Semantic Web Conference (ISWC). Springer, 2015.
  [[PDF](http://arxiv.org/pdf/1411.2749.pdf)]

The code for the initial study can be found here:

- [trustypublishing-study](https://bitbucket.org/tkuhn/trustypublishing-study/)

