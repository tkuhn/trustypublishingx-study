#!/usr/bin/r
#
# to run this script type:
# r eval.R

zurichdata = read.csv("zurich.csv", header=TRUE)
ottawadata = read.csv("ottawa.csv", header=TRUE)

pdf("plot.pdf", width=10, height=4.5)
par(mfrow = c(2, 1))
par(mar=c(3.5,6,0.5,1))
boxplot(zurichdata,
  horizontal=TRUE,
  ylim=c(0, 1000),
  las=1,
  names=c("Ottawa\n(6129 km)", "New Haven\n(6212 km)", "Zurich\n(0 km)"),
  xlab="response time (in milliseconds) from monitor in Zurich\n",
  col=rgb(0,0,0.7,0.5),
  pars=list(outcol=rgb(0.4,0.4,0.7,0.3),outcex=1)
)
boxplot(ottawadata,
  horizontal=TRUE,
  ylim=c(0, 1000),
  las=1,
  names=c("Zurich\n(6129 km)", "New Haven\n(507 km)", "Ottawa\n(0 km)"),
  xlab="response time (in milliseconds) from monitor in Ottawa\n",
  col=rgb(0,0,0.7,0.5),
  pars=list(outcol=rgb(0.4,0.4,0.7,0.3),outcex=1)
)

